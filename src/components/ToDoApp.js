import React, {useState} from "react";
import { v4 as uuidv4 } from 'uuid';


const AddToDO = (props) => {
    return(<button onClick={() => {props.onClick()}}>Add TODO</button>);
}

const ToDoItem = (props) => {
    return(
        <label htmlFor={props.data.id} style={{textDecoration:!props.data.active ? "line-through":"" }}>
            <input type="checkbox"
                   id={props.data.id}
                   checked={!props.data.active}
                   onChange={(event) => {props.onChange(event.target.checked, props.data.id)}} />
            {props.data.description}
        </label>
    );
}

const ToDoApp = () => {
    const [toDOList, setToDOList] = useState([]);
    const [toDOInput, setToDOInput] = useState('');
    const [display, setDisplay] = useState('all');
    const onChange = (checked, id) => {
        for (let i = 0; i < toDOList.length; i++) {
            if(toDOList[i].id === id) {
                toDOList[i].active = !checked;
            }
        }
        setToDOList([...toDOList]);
    };

    const deleteAllDone = () => {
        setToDOList(toDOList.filter(el => el.active));
    };

    const onClick = () => {
        if (toDOInput !== '') {

            const newToDO = {
                "id": uuidv4(),
                "description":toDOInput,
                active: true
            }
            toDOList.push(newToDO);
            console.log("onClick", toDOInput, toDOList);
            setToDOList([...toDOList]);
        }
        setToDOInput('');
    };
    return (<div>
        <h1>TODO List</h1>
        <fieldset className="checkboxgroup">
            {toDOList.filter(el => display==='active'? el.active: display==='completed'? !el.active: el ).map(el => <ToDoItem key={el.id} data={el} onChange={onChange}/> )}
        </fieldset>
        <div className="inputTODO">
            <input placeholder="What TODO?" value={toDOInput} onChange={(event)=>{setToDOInput(event.target.value)}}/>
            <AddToDO onClick={onClick}/>
        </div>
        <div className="controlBtns">
            Show:
            <button className={display==='all'?"active":""} onClick={()=>{setDisplay('all')}}>All</button>
            <button className={display==='active'?"active":""} onClick={()=>(setDisplay('active'))}>Active</button>
            <button className={display==='completed'?"active":""} onClick={()=> (setDisplay('completed'))}>Completed</button>
        </div>
        <div className="controlBtns">
            <button onClick={()=>{deleteAllDone()}}>Delete All Completed</button>
        </div>
        <div className="message">TODOs left: {toDOList.filter(el => el.active).length}</div>
    </div>);
};

export default ToDoApp;